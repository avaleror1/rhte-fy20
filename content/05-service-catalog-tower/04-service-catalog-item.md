+++
title = "Create Service Catalog Item"
weight = 4
+++

Now we can start adding Service Catalog Items to the previously created Service Catalog.

### Create a Service Catalog Item

In the following step we create a Service Catalog Item which will execute an Ansible Playbook.

- Navigate to ***Services*** -> ***Catalogs***.

{{% notice note %}}
If you followed the instructions by the letter, you're already in this part of the UI.
{{% /notice %}}

- Navigate to ***Catalog Items*** in the accordion on the left.

- Click on ***Configuration*** -> ***Add a New Catalog Item***.

***Catalog Item Type:*** Ansible Tower

***Name:*** Install Package

***Description:*** Install Package via Ansible Playbook

***Display in Catalog:*** yes

***Catalog:*** My Company/Ansible Tower

***Dialog:*** InstallPackageJob

***Zone:*** Default Zone

***Provider:*** Ansible Tower Automation Manager

***Ansible Tower Template:*** InstallPackage

- Click ***Add*** to create the Service Catalog Item.

Leave the "Provisioning Entry Point" as it is or otherwise the hook into the backend logic is broken and the order will fail to execute.

{{% notice info %}}
On the "Details" tab additional information can be provided for the end user when ordering this Service Catalog Item. You can use simple HTML formatting in this field as well.
{{% /notice %}}
