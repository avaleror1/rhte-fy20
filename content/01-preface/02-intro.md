+++
title = "Introduction and Lab Access"
weight = 2
+++

This chapter will give you some context of the intention of writing these labs, on how to use them and how to provide feedback.

### How to access the lab environment

Each student will have a dedicated lab environment. Each lab has a unique GUID. To get access to your lab, use the following instructions.

![GUID Landing page](/img/guid-landing-page.png)

- Navigate to the GUID Grabber: [https://www.opentlc.com/gg/gg.cgi?profile=generic_rhte](https://www.opentlc.com/gg/gg.cgi?profile=generic_rhte)

- Lab Code: R2000 - CloudForms and Ansible Deep Dive

- Activation Key: cfansible

- E-Mail Address: &lt;your email address&gt;

{{% notice note %}}
Please do use a proper email address! We're not sending mails to it and delete it right after the lab is over. The email address is only used to make sure no two students get the same lab environment assigned.
{{% /notice %}}

After you provided the data and clicked on ***Submit*** you'll see a page with some details about how to access the lab and you individual GUID:

![GUID Details](/img/cfansible-guid.png)

During the lab we will only interact with CloudForms Web UI.

{{% notice info %}}
Remember your GUID - maybe you want to write it down or make sure you keep the GUID Grabber page open in a specific browser window or tab.
{{% /notice %}}

With the GUID you can now connect to CloudForms:

    https://cfui-GUID.rhpds.opentlc.com

Replace GUID with your own specific GUID. If your GUID is 1234, the URL will be:

    https://cfui-1234.rhpds.opentlc.com

***Username:*** admin

***Password:*** r3dh4t1!

Feel free to continue reading or jump to the next chapter and start with the [actual lab](../../02-configure-embedded/).

### How it all started

There are several places on the internet to find information about [CloudForms](http://www.redhat.com/cloudforms/) and [ManageIQ](http://www.manageiq.org). A good start is of course the [CloudForms Product Documentation](https://access.redhat.com/documentation/en-us/red_hat_cloudforms) or the [ManageIQ Project Documentation](http://manageiq.org/docs/). The [Red Hat CloudForms Blog](http://cloudformsblog.redhat.com) provides additional details and articles on how to use CloudForms. There is also a [ManageIQ Blog](http://manageiq.org/blog/) to follow news of the upstream project.

To learn everything there is to know about Automate, Peter McGowan's [Mastering CloudForms Automate](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/) and the [Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) are great resources.

On top of that you can find articles on [Christian Jung's Blog](http://www.jung-christian.de) or the [TigerIQ Blog](http://tigeriq.co).

What is missing though is a consolidated source for Ansible related topics. Although the Ansible integration is improving with each release and constantly becomes easier to use, there are certain advanced topics which are not very well documented. It's also often useful to have all information in a consolidated sources.

This Lab is an attempt to provide such documentation. There is a series of blog posts which give you the theoretical background to understand how Ansible and CloudForms work together (they have not been published yet). This collection of labs will give you the hands on experience to use the theoretical knowledge in real world scenarios.

### How to run through this lab

Although it probably makes sense to run the lab in sequential order, each lab is supposed to be modular. This means, you can choose any of the top level chapters and run them in any particular order. For example, if you're only interested in how things work with embedded Ansible, you can skip the Tower related chapters entirely. That said, there are still some dependencies. You can not perform any of the Ansible Tower related labs, until you configured the Tower provider. There are indeed certain expectations towards the intelligence of the reader.

### How to build your own lab environment

Prerequisites:

- CloudForms Appliance with at least one Infrastructure or Cloud Provider

- Internet access from CloudForms to use git repositories

- Ansible Tower for the Tower related labs (or AWX?)
