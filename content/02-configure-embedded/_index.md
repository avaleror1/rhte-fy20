+++
title = "Configure Embedded Ansible"
weight = 2
chapter = true
+++

# Configure Embedded Ansible

Learn some basics of CloudForms, how to build a Service Catalog and leverage the power of Ansible.
