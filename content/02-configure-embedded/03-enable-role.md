+++
title = "Enable Embedded Ansible"
weight = 3
+++

This part of the lab will guide you through the process of setting up an Service Catalog Item which runs an Ansible Playbook.

### Make sure Embedded Ansible role is enabled and running

Before we continue, we want to make sure the Embedded Ansible role is enabled and running.

- Click on ***Configuration*** icon next to your user name on the top right corner of the UI.

{{% notice info %}}
Starting with CloudForms 5.0, "Configuration" is no longer a sub menu when clicking on the username.
{{% /notice %}}

- Make sure the "Embedded Ansible" Roles is enabled.

- If the role is not enabled, turn it on and click on ***Save***.

Starting with CloudForms 5.0, [Ansible Runner](https://github.com/ansible/ansible-runner) is used which improves performance significantly. A popup notification will inform you, after the role was enabled.
