+++
title = "Create the Ansible Tower Method VM Info"
weight = 3
+++

### Create the Ansible Tower Job Method "VM Info"

In this part of the lab we demonstrate how to run an Ansible Tower Job from an Automate State Machine.

- Click on the ***Methods*** Class and then the ***Methods*** tab in the right part of the window.

- Create a new Method to run a Ansible Tower Job Template. Click on ***Configuration*** -> ***Add a new Method***.

- Switch the Method Type to "Ansible Tower Job Template"

- Use the following details to fill out the form.

***Name:*** vm_info

***Display Name:*** Get and modify info from provisioning object

***Provider:*** Ansible Tower Automation Manager

***Job Template:*** VM info

***Logging Output:*** On Error

- Click ***Add*** to create the Ansible Tower Job Template Method.

### Create an Instance for the Ansible Tower Method

To be able to call the Method from a State Machine, we need an associated Instance.

- In the right part of the window, click on the ***Instances*** tab.

- Click on ***Configuration*** -> ***Add a new Instance***.

- Enter the following details into the Dialog:

***Name:*** vm_info

***Display Name:*** Get and modify provisioning info

***Fields:*** In the table search the row "execute" and put "vm_info" into the "value" field.

- Click ***Add*** to save the Instance.
