+++
title = "Customizing the State Machine"
weight = 1
+++

In this part of the lab, you will perform a series of tasks to customize the Provisioning State Machine of CloudForms. To better understand the following steps a quick introduction and overview of the process might be useful.

### Customizing CloudForms

CloudForms provides two mechanisms for backend customization. Since CloudForms is written in Ruby we can use Ruby Methods for our changes. The awesome [Mastering Automation in CloudForms](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/) and [Mastering Automation in CloudForms Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) are excellent sources to learn everything about these capabilities.

Since Red Hat acquired [Ansible](http://www.ansible.com) the integration with CloudForms is constantly improved. We can use Ansible Playbooks, Ansible Tower Jobs and Workflows in the Service Catalog, in the Automate State Machine and several other areas.

### What's a State Machine

A [State Machine](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/state_machines/chapter.html) describes a series of tasks which have to be executed. There are a number of State Machines as part of the product. For this lab, we will customize the [Provisioning State Machine](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/provisioning_a_virtual_machine/chapter.html).

### Using Ansible Tower Jobs

Starting with CloudForms 4.6 we can create Methods of type "Playbook" which, instead of running Ruby code, execute an Ansible Playbook. Staring in CloudForms 5.0, you can also use Ansible Tower Job Templates. Two different Tower Job Templates will be used in this lab.

[Job Template - "VM info"](https://gitlab.com/redhat-cop/mbu-lab/ansible/blob/master/vmdb-automate.yml): This Job Template will access the provisioning request object in the CloudForms Automate Workspace using the [syncrou.manageiq-vmdb](https://github.com/syncrou/manageiq-vmdb) role to retrieve the VM name. After that it will build the fully qualified domain name of the host, which will be required by Satellite during the registration process. The part of the Job Template will use the [syncrou.manageiq-automate](https://github.com/syncrou/manageiq-automate) role to create a state_var inside the Automate Workspace.

[Job Template - "Satellite Registration"](https://gitlab.com/redhat-cop/mbu-lab/ansible/blob/master/sat_vmregister.yml): Running this Playbook once the VM is provisioned, will register it with the Satellite Server. Remember that we tweaked the CheckProvisioned method in order to make sure that the VM is up and running before running this Playbooks.

Inside a CloudForms appliance the roles we're using in this lab are already preinstalled, but named manageiq-core.manageiq-vmdb and manageiq-core.manageiq-automate. In CloudForms 5.0 the user is able to pass variables between Ansible and CloudForms Automate and vice versa. From CloudForms' perspective any state_var can be accessed by a Playbook and from a Playbook you can use set_facts to store variables that can be access from CloudForms Automate. These capabilities can also be used to pass variables between different Ansible Playbooks and Ansible Tower Jobs.

If you want to know more about the new features and the roles, take a look at the [CloudForms 5.0 Enablement presentation](https://docs.google.com/presentation/d/1JBmJhRyehB-kKx5_ZSQSh_uJm3Z0mnVV86j6jUQXSYk/edit#slide=id.g606d6a429a_0_221) (you will need an Red Hat internal login to access this document).

### The use case

A very common integration task is customize CloudForms to register new provisioned VMs in Satellite in the same provisioning State Machine.

In the lab environment an instance of Satellite is deployed and can be used. The following chapters will guide you through the process of implementing the necessary changes.

1. [Prerequisites](../02-prerequisites/): If you have done other chapters of this lab before, it is necessary disable the previously created Automate Domain.

1. [Create the VM Info Method](../03-vm_info-method/): A new method based on an Ansible Tower Job Template will be created in order to interact with CloudForms.

1. [Create the Satellite Registration Method](../04-sat_reg-method/): A new method based on an Ansible Tower Job Template will be created for registering VMs into Satellite as part of the provisioning state machine.

1. [Edit the State Machine](../05-edit-sm/): After the new methods has been created is is needed to place them into new states inside states machine with the proper order.

1. [Test the custom State Machine](../06-test-custom-sm/): The steps needed to test your custom state machine and make sure that the results are the expected ones.
