+++
title = "Prerequisites"
weight = 2
+++

### Disable Lab Domain

For the sake of clearness and not to get confused with the previous work, the "Lab" domain is going to be disabled before creating a new one.

- Navigate to - Navigate to ***Automation*** -> ***Automate*** -> ***Explorer***.

- Click on the "Lab" domain and then go to ***Configuration*** -> ***Edit this Domain***.

- Remove the mark from Enabled and click ***Save***.

Now you´re good to go and create the new domain needed for this lab.

### Create a new Automate Domain

Since the Automate Domains shipped with CloudForms are read only, we have to create our own Domain first.

- Click on the root object "Datastore"

- Click on ***Configuration*** -> ***Add a new Domain***.

- Enter the following details:

***Name:*** RHTE

***Description:*** RHTE FY20

***Enabled:*** Check

- Click ***Add***.

### Copy VM Provisioning State Machine

To be able to make changes to the State Machine, we have to copy it to our writeable Domain first.

- Navigate to the VM Provisioning State Machine in the "Datastore" tree:

***ManageIQ*** -> ***Infrastructure*** -> ***VM*** -> ***Provisioning*** -> ***StateMachines*** -> ***VMProvision_VM*** -> ***Provision VM from Template***

- Click on ***Configuration*** -> ***Copy this Instance***.

{{% notice warning %}}
Make sure you highlight the "Provision VM from Template (template)" instance when initiating the copy!
{{% /notice %}}

- Accept the defaults when confirming the copy.

- Click ***Copy*** to confirm.

- After the copy was completed, you should see a confirmation page.

### Copy the CheckProvisioned Method

We have to modify the CheckProvisioned Method to make sure it waits until the VM is up and running.

- Navigate to the CheckProvisioned Method in the "Datastore" tree:

***ManageIQ*** -> ***Infrastructure*** -> ***VM*** -> ***Provisioning*** -> ***StateMachines*** -> ***Methods*** -> ***CheckProvisioned (check_provisioned)***

{{% notice note %}}
Make sure you click on the Method (which has a red diamond icon) and not on the instance (which has a grey paper icon).
{{% /notice %}}

- Click on ***Configuration*** -> ***Copy this Method***.

- Accept the defaults when confirming the copy.

- Click ***Copy*** to confirm.

### Modify the CheckProvisioned Method

After the VM has been created we want to make sure that Ansible is able to connect to it before trying to run Playbooks or Job Templates on it. This is the reason why the CheckProvisioned Method has to be customized after copying it.

- Navigate to the recently copied method and click on it.

- Click on ***Configuration*** -> ***Edit this Method***

- Paste the next code at the end of the method:

        vm = task.vm
        $evm.log("info","Current IP Addresses: #{vm.ipaddresses}") unless vm.nil?
        $evm.log("info", "VM is still nil") if vm.nil?

        if not vm.nil?
            if task.destination.ipaddresses.empty?
                $evm.root['ae_result']         = 'retry'
                $evm.root['ae_retry_interval'] = '1.minute'
            end
        end

{{% notice warning %}}
Make sure you extend the existing method by adding the code to the end. Do not replace or remove any of the existing lines of code!
{{% /notice %}}

- Click ***Validate*** to verify the code has no syntax errors.

- Click ***Save*** to store all changes.
