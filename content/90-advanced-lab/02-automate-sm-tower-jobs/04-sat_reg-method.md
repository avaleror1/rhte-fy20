+++
title = "Create the Ansible Tower Method Satellite Registration"
weight = 4
+++

### Create the Ansible Tower Method "Satellite Registration"

This section will setup a second Ansible Tower Job which is executed from the CloudForms Automate State Machine.

- Click on the ***Methods*** Class and then the ***Methods*** tab in the right part of the window.

- Create a new Method to run an Ansible Playbook. Click on ***Configuration*** -> ***Add a new Method***.

- Switch the Method Type to "Ansible Tower Job Template".

- Use the following details to fill out the form.

***Name:*** sat_register

***Display Name:*** Register a VM in Satellite

***Provider:*** Ansible Tower Automation Manager

***Job Template:*** Satellite Registration

***Logging Output:*** On Error

- Click ***Add*** to create the Ansible Tower Job Template Method.

### Create Instances for the Ansible Tower Method "Satellite Registration"

To be able to call the Method from a State Machine, we need an associated Instance.

- In the right part of the window, click on the ***Instances*** tab.

- Click on ***Configuration*** -> ***Add a new Instance***

- Enter the following details into the Dialog:

***Name:*** sat_register

***Display Name:*** Register a VM in Satellite

***Fields:*** In the table search the row "execute" and put "sat_register" into the "value" field.

- Click ***Add*** to save the Instance.
