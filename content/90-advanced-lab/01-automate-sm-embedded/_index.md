+++
title = "Automate State Machines with Embedded Ansible"
weight = 9
chapter = true
+++

# Automate State Machines with Embedded Ansible

In this advanced lab you will practice how to run Ansible Playbook Methods from an Automate State Machine.
