+++
title = "Send Variable to Playbook"
weight = 5
+++

### Send a variable to an Ansible Playbook

A typical use case is to implement integration with external systems by utilizing Ansible. In this example we want to create a new device record in phpIPAM by using an Ansible Playbook and variables which already exist in CloudForms.

### Create the Ruby Method

- Click on the ***Methods*** Class in the "Datastore" tree and then the ***Methods*** tab in the right part of the window.

- Create a new Method to run a Ruby Method. Click on ***Configuration*** -> ***Add a new Method***.

- Switch the Method Type to "Inline".

- Use the following details to fill out the form.

***Name:*** send_variable

***Display Name:*** send variable to Ansible Playbook

Replace the existing code with the following lines:

    prov=$evm.root["miq_provision"]

    hostname=prov.get_option(:vm_target_hostname)
    ipaddr=prov.get_option(:ip_addr)

    $evm.set_state_var(:ansible_stats_hostname, hostname)
    $evm.set_state_var(:ansible_stats_ip_addr, ipaddr)

- Click ***Validate*** to verify there is no syntax error in the Ruby Code. The result of the check will be shown on the top of the page.

- Click ***Add*** to create save the Ruby Method.

### Create the Instance

To be able to call the Method from a StateMachine, we need an associated Instance.

- Click on the ***Instances*** tab and ***Configuration*** -> ***Add a new Instance***.

- Enter the following details into the Dialog:

***Name:*** send_variable

***Display Name:*** send variable to Ansible Playbook

***Fields:*** In the table search the row "execute" and put "send_variable" into the "value" field.

- Click ***Add*** to save the Instance.

### Modify the State Machine

To run the Ansible Playbook during Virtual Machine Provisioning, we have to change the "RegisterDHCP" state to the State Machine.

- Click on the "Provision VM from Template" Instance.

- Click on ***Configuration*** -> ***Edit this Instance***.

- Edit the row "RegisterDHCP" with the following details:

***Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/send_variable

- Click ***Save*** to save the new Schema.

{{% notice note %}}
We use an existing placeholder state for our new Ruby Method. It would be more clean to modify the State Machine, but for the purpose of this lab, we want to keep things simple.
{{% /notice %}}
